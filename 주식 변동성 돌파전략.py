import pandas_datareader.data as web
import datetime
import pandas as pd
import numpy as np

K = 0.6


start = datetime.datetime(2017, 9, 1)

end = datetime.datetime(2020, 9, 1)


# 코스피
#item_code = '035420'        # 네이버

item_code = '005930'        # 삼성전자

#item_code = '007575'        #일양약품우

#item_code = '233740'        #코덱스 150 레버리지

item_code = '253280'        #KBSTAR 헬스케어(수익 나는 거 확인)

#item_code = '068270'        #셀트리온

#item_code = '035720'        #카카오

#item_code = '207940'        #삼성바이오로직스


#item_code = '007570'

#코스닥
#item_code = '091990'		

df = web.DataReader(item_code + '.KS', 'yahoo', start, end)

#df = web.DataReader(item_code + '.KQ', 'yahoo', start, end)

#df = web.DataReader('BTC-USD', 'yahoo', start, end)

#df = web.DataReader('TSLA', 'yahoo', start, end)

print('Code : ' + str(item_code) )

fee = 0.0032
#fee = 0.002

print('Fee :' + str(fee * 100) + '%')

print(df)

df['Range'] = (df['High'] - df['Low']) * K
df['Target'] = df['Open'] + df['Range'].shift(1)


df['Sell'] = df['Open'].shift(-1)           #매도가

#df['Sell'] = df['Close']           			#매도가

#df['Target'] = df['Close'] + df['Range'].shift(1)



#df['ROR'] = np.where(df['High'] > df['Target'], df['Close'] / df['Target'] - fee, 1)

df['ROR'] = np.where(df['High'] > df['Target'], df['Sell'] / df['Target'] - fee, 1)


df['HPR'] = df['ROR'].cumprod()

df['DD'] = (df['HPR'].cummax() - df['HPR']) / df['HPR'].cummax() * 100

print("MDD: ", df['DD'].max())

print("HPR: ", df['HPR'][-2])

win = np.count_nonzero(df['ROR'] > 1)

lose = np.count_nonzero(df['ROR'] < 1)

POV = round(win/(win + lose) * 100, 1)


print("Percentages of Victories : %d%%(Win:%d, Lose:%d)"%(POV,win,lose))

df.to_excel("data.xlsx")
